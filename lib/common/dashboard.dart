import 'package:dekornata_test/common/pages/bag.dart';
import 'package:dekornata_test/common/pages/favorite.dart';
import 'package:dekornata_test/common/pages/home.dart';
import 'package:dekornata_test/common/pages/profile.dart';
import 'package:dekornata_test/common/pages/shop.dart';
import 'package:dekornata_test/constants.dart';
import 'package:flutter/material.dart';

class Dashboard extends StatefulWidget {
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  int _selectedIndex = 1;
  final List<Widget> _widgetOptions = <Widget>[
    Home(),
    Shop(),
    Bag(),
    Favorite(),
    Profile(),
  ];
  void _onItemPressed(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
          child: _widgetOptions.elementAt(_selectedIndex),
        ),
        bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          backgroundColor: Colors.white,
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              title: Text('Home'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.shopping_cart),
              title: Text('Shop'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.shopping_basket),
              title: Text('Bag'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.favorite),
              title: Text('Favorite'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.person),
              title: Text('Profile'),
            ),
          ],
          currentIndex: _selectedIndex,
          selectedItemColor: const Color(Contstants.primaryThemeRed),
          onTap: _onItemPressed,
        ),
    );
  }
}
