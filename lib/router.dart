import 'package:dekornata_test/materials/filter_page.dart';
import 'package:dekornata_test/materials/filter_product.dart';
import 'package:flutter/material.dart';
import 'package:dekornata_test/common/dashboard.dart';

checkIfAuthenticated() async {
  await Future.delayed(Duration(
      seconds: 5)); // could be a long running task, like a fetch from keychain
  return true;
}

class Router {
  static Route<dynamic> onGenerateRoute(RouteSettings settings) {
    final args = settings.arguments; //passing value/argument
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (_) => Dashboard());
        break;
      case '/filter':
        return MaterialPageRoute(builder: (_) => FilterPage());
        case '/filter-product':
        return MaterialPageRoute(builder: (_) => FilterProduct(
          filter : args
        ));
      default :
        return MaterialPageRoute(builder: (_) => Dashboard());
    }
  }
}


