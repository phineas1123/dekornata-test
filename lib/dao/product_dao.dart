import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dekornata_test/model/filter.dart';

// import 'package:firebase_storage/firebase_storage.dart';

class ProductDao {
  final CollectionReference collection =
      Firestore.instance.collection('products');

  Future getProducts() async {
    QuerySnapshot p = await collection.getDocuments();
    return p.documents;
  }
  //flutter tidak support multiple condition
  Future getProductsWithFilters(Filter filter) async {
    print(filter.category);
    QuerySnapshot p;
    if (filter.category == "All")
      p = await collection
          .where("price", isGreaterThanOrEqualTo: filter.price.start)
          .where("price", isLessThanOrEqualTo: filter.price.end)
          // // .where("color", whereIn: filter.color)
          // .where("size", whereIn: filter.size)
          .getDocuments();
    else
      p = await collection
          .where("price", isGreaterThanOrEqualTo: filter.price.start)
          .where("price", isLessThanOrEqualTo: filter.price.end)
          // // .where("color", whereIn: filter.color)
          // .where("size", whereIn: filter.size)
          // .where("category",arrayContains: filter.category)
          .getDocuments();
    print(p.documents);
    return p.documents;
  }
}
