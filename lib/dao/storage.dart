import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
class CustomStorage {
  final FirebaseStorage storage = FirebaseStorage(
      app: Firestore.instance.app,
      storageBucket: 'gs://dekornata-test-firebase.appspot.com');  

   Future<String> getImagePath(String path) async {
    // QuerySnapshot p =  await collection.getDocuments();
    var ref = FirebaseStorage.instance.ref().child(path);
    var url = await ref.getDownloadURL();

    print(url.toString());

    return url;
  }


}
