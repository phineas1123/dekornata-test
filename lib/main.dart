import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:dekornata_test/router.dart';

void main() {
  //prevent screen rotation
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations(
          [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown])
      .then((_) => runApp(new Application()));
}

class Application extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        fontFamily: 'Arial',
        primaryColor: Color(0xfff9f9f9),
      ),
      initialRoute: '/',
      onGenerateRoute: Router.onGenerateRoute,
    );
  }
}
