import 'package:dekornata_test/dao/product_dao.dart';
import 'package:dekornata_test/dao/storage.dart';
import 'package:dekornata_test/model/filter.dart';
import 'package:flutter/material.dart';

class FilterProduct extends StatefulWidget {
  final Filter filter;

  const FilterProduct({Key key, this.filter}) : super(key: key);

  @override
  _FilterProductState createState() => _FilterProductState();
}

class _FilterProductState extends State<FilterProduct> {
  @override
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.filter.category +"\'s top"),
        centerTitle: true,
        actions: [
          Icon(Icons.search),
        ],
      ),
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            _buildHeader(context),
            _buildContent(context),
          ],
        ),
      ),
    );
  }

  Container _buildHeader(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Column(
        children: <Widget>[
          _customContainer(
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  _categoryBar("T-shirts"),
                  _categoryBar("Crop tops"),
                  _categoryBar("Blouse"),
                  _categoryBar("Dress"),
                ],
              ),
            ),
          ),
          _customContainer(
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                FlatButton.icon(
                  onPressed: () => Navigator.pushNamed(context, '/filter'),
                  icon: Icon(Icons.filter_list),
                  label: Text("Filter"),
                ),
                FlatButton.icon(
                  onPressed: () {},
                  icon: Icon(Icons.swap_vert),
                  label: Text("Price Lowest to High"),
                ),
                IconButton(icon: Icon(Icons.view_list), onPressed: null)
              ],
            ),
          ),
        ],
      ),
    );
  }

  Container _buildContent(BuildContext context) {
    var size = MediaQuery.of(context).size;
    final double itemHeight = (size.height - kToolbarHeight - 24) / 2;
    final double itemWidth = size.width / 2;
    return Container(
      child: FutureBuilder(
        future: ProductDao().getProductsWithFilters(widget.filter),
        builder: (_, snapshot) {
          if (snapshot.connectionState == ConnectionState.done)
            return GridView.builder(
              scrollDirection: Axis.vertical,
              shrinkWrap: true, //dynamic items
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                childAspectRatio: itemWidth / itemHeight,
              ),
              itemCount: snapshot.data.length,
              itemBuilder: (_, index) {
                return Container(
                  // color: Colors.red,
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Expanded(
                        child: ClipRRect(
                          borderRadius: BorderRadius.all(Radius.circular(30)),
                          child: AspectRatio(
                            aspectRatio: 16 / 10,
                            child: FutureBuilder(
                                future: CustomStorage().getImagePath(
                                    "/products/" +
                                        snapshot.data[index].data["image"]),
                                builder: (_, snapshot2) {
                                  if (snapshot2.connectionState ==
                                      ConnectionState.done)
                                    return Container(
                                      decoration: BoxDecoration(
                                        image: DecorationImage(
                                          fit: BoxFit.fill,
                                          image: NetworkImage(snapshot2.data),
                                        ),
                                      ),
                                    );
                                  else
                                    return Center(
                                      child: Text('Loading'),
                                    );
                                }),
                          ),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Row(
                            children: List.generate(
                              5,
                              (i) {
                                return Icon(
                                    i < snapshot.data[index].data["rating"]
                                        ? Icons.star
                                        : Icons.star_border);
                              },
                            ),
                          ),
                          Text("(" +
                              snapshot.data[index].data["review"].toString() +
                              ")"),
                        ],
                      ),
                      Text(snapshot.data[index].data["brand"]),
                      Text(
                        snapshot.data[index].data["name"],
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 20),
                      ),
                      Text(
                        snapshot.data[index].data["price"].toString() + "\$",
                        style: TextStyle(
                          fontSize: 18,
                        ),
                      ),
                    ],
                  ),
                );
              },
            );
          else
            return Center(
              child: Text('Loading'),
            );
        },
      ),
    );
  }

  Container _categoryBar(String text) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 5),
      padding: EdgeInsets.symmetric(horizontal: 30, vertical: 10),
      decoration: BoxDecoration(
        color: Colors.black,
        borderRadius: BorderRadius.circular(20),
      ),
      child: Text(
        text,
        style: TextStyle(color: Colors.white),
      ),
    );
  }

  Container _customContainer(Widget widget) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      width: double.infinity,
      child: widget,
    );
  }
}
