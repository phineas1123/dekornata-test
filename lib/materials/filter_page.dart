import 'package:dekornata_test/constants.dart';
import 'package:dekornata_test/model/filter.dart';
import 'package:flutter/material.dart';

class FilterPage extends StatefulWidget {
  @override
  _FilterPageState createState() => _FilterPageState();
}

class _FilterPageState extends State<FilterPage> {
  var _priceValue = RangeValues(78, 143);
  var _colorIdx = [-1, -1, -1, -1, -1, -1];
  var _colorValue = [
    Colors.black,
    Colors.grey,
    Colors.red,
    Colors.purpleAccent,
    Colors.brown,
    Colors.blue
  ];

  var _sizeIdx = [-1, -1, -1, -1, -1];
  var _sizeValue = ["XS", "S", "M", "L", "XL"];

   var _categoryIdx = 0;
  var _categoryValue = ["All", "Women", "Men", "Boys", "Girls"];

  _apply() {

    List<String> colorValue = new List();
    List<String> sizeValue = new List();
    
    List.generate(_colorValue.length, (index) {
      if(_colorIdx[index] != -1){
          colorValue.add(_colorValue[index].toString());
      }
    });

    List.generate(_sizeValue.length, (index) {
      if(_sizeIdx[index] != -1){
          sizeValue.add(_sizeValue[index]);
      }
    });
    Filter filter = new Filter(
      _priceValue,
      colorValue,
      sizeValue,
      _categoryValue[_categoryIdx]
    );

    Navigator.pushNamed(context, "/filter-product",arguments: filter);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Filters"),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios, color: Colors.black),
          onPressed: () => Navigator.of(context).pop(),
        ),
        centerTitle: true,
      ),
      body: SafeArea(
        child: Container(
          height: double.infinity,
          child: Stack(
            alignment: Alignment.bottomCenter,
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(bottom: 80),
                height: double.infinity,
                child: SingleChildScrollView(
                  scrollDirection: Axis.vertical,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      _filterElement(
                        "Price Range",
                        _priceRange(),
                      ),
                      _filterElement(
                        "Colors",
                        _colors(),
                      ),
                      _filterElement(
                        "Sizes",
                        _sizes(),
                      ),
                      _filterElement(
                        "Category",
                        _category(),
                      ),
                      _filterElement(
                        "Brand",
                        _brand(),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.all(20),
                decoration: BoxDecoration(
                  color: const Color(Contstants.primaryThemeWhite),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 5,
                      blurRadius: 7,
                      offset: Offset(0, 3), // changes position of shadow
                    ),
                  ],
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    _secondaryButton(
                      "Discard",
                      () => Navigator.pop(context),
                      RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20.0),
                        side: BorderSide(color: Colors.black),
                      ),
                    ),
                    _primaryButton(
                      "Apply",
                      () => _apply(),
                      RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  RaisedButton _secondaryButton(String text, Function func,
      [ShapeBorder shapeBorder]) {
    return RaisedButton(
      onPressed: func,
      child: Text(
        text,
        style: TextStyle(color: Colors.black),
      ),
      padding: EdgeInsets.symmetric(horizontal: 50),
      color: const Color(Contstants.primaryThemeWhite),
      shape: shapeBorder,
    );
  }

  RaisedButton _primaryButton(String text, Function func,
      [ShapeBorder shapeBorder]) {
    return RaisedButton(
      onPressed: func,
      child: Text(
        text,
        style: TextStyle(color: Colors.white),
      ),
      padding: EdgeInsets.symmetric(horizontal: 50),
      color: const Color(Contstants.primaryThemeRed),
      shape: shapeBorder,
    );
  }

  Container _filterElement(String text, Widget widget) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 20),
      // width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: Text(
              text,
              style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: 18),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            padding: EdgeInsets.all(20),
            decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.3),
                  spreadRadius: 1,
                  blurRadius: 1,
                  offset: Offset(0, 3), // changes position of shadow
                ),
              ],
            ),
            child: widget,
          ),
        ],
      ),
    );
  }

  _priceRange() {
    return Row(
      children: <Widget>[
        Container(
          width: 60,
          child: Text(
            "\$" + _priceValue.start.round().toString(),
            style: TextStyle(fontSize: 18),
          ),
        ),
        Expanded(
          child: RangeSlider(
            values: _priceValue,
            min: 0,
            max: 1000,
            labels: RangeLabels('\$${_priceValue.start.round().toString()}',
                '\$${_priceValue.end.round().toString()}'),
            divisions: 1000,
            onChanged: (RangeValues newPriceValue) {
              setState(() {
                // print(_priceValue.start);
                if (newPriceValue.end - newPriceValue.start >= 20) {
                  _priceValue = newPriceValue;
                }
              });
            },
          ),
        ),
        Container(
          width: 60,
          child: Text(
            "\$" + _priceValue.end.round().toString(),
            style: TextStyle(fontSize: 18),
          ),
        ),
      ],
    );
  }

  _colors() {
    int colorCount = _colorValue.length;

    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        children: List.generate(
          colorCount,
          (i) => new MaterialButton(
            height: _colorIdx[i] == 1 ? 40 : 35,
            onPressed: () {
              setState(() {
                _colorIdx[i] = -(_colorIdx[i]);
              });
            },
            color: _colorValue[i],
            shape: CircleBorder(
              side: _colorIdx[i] == 1
                  ? BorderSide(color: Colors.red, width: 3)
                  : BorderSide.none,
            ),
          ),
        ),
      ),
    );
  }

  _sizes() {
    int sizeCount = _sizeValue.length;
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: List.generate(
          sizeCount,
          (i) => new Container(
            margin: EdgeInsets.symmetric(horizontal: 10),
            child: _sizeIdx[i] == 1
                ? _primaryButton(_sizeValue[i], () {
                    setState(() {
                      _sizeIdx[i] = -(_sizeIdx[i]);
                    });
                  })
                : _secondaryButton(_sizeValue[i], () {
                    setState(() {
                      _sizeIdx[i] = -(_sizeIdx[i]);
                    });
                  }),
          ),
        ),
      ),
    );
  }
  
  _category() {
    int categoryCount = _categoryValue.length;
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: List.generate(
          categoryCount,
          (i) => new Container(
            margin: EdgeInsets.symmetric(horizontal: 10),
            child: _categoryIdx == i
                ? _primaryButton(_categoryValue[i], () {
                    setState(() {
                      _categoryIdx = i;
                    });
                  })
                : _secondaryButton(_categoryValue[i], () {
                    setState(() {
                      _categoryIdx = i;
                    });
                  }),
          ),
        ),
      ),
    );
  }

  _brand(){

  }
}
