# dekornata_test

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

Feature:
- Shop Page
- Filter Page
- Product after Filter

Currently Supported Platform:
- Android

Tools : 
- Android Studio
- Firebase
- Visual Studio Code
- Google Pixel 2 XL (Emulator)

Setup Flutter at VsCode: https://flutter.dev/docs/development/tools/vs-code

Current Firebase: dekornata-test-firebase

Missing Feature:
- Filter Product by multiple params (Price, Color, Category, Size, Brand) => Firestore didn't support multiple condition

